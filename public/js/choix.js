function sauvegarderChoix(choix, key, url) {
    // Appel AJAX pour enregistrer le choix dans la session Symfony
    fetch(url, {
        method: 'POST',
        body: JSON.stringify({ choix: choix, key: key }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Erreur HTTP ' + response.status);
        }
        return response.json();
    })
    .catch(error => {
        console.error('Erreur lors de la requête AJAX:', error);
    });
}