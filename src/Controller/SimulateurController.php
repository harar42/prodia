<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class SimulateurController extends AbstractController
{
    #[Route('/enregistrer-choix-utilisateur', name: 'enregistrer_choix_utilisateur', methods: ['POST'])]
    public function enregistrerChoixUtilisateur(Request $request): JsonResponse
    {
        // Récupérer les données JSON envoyées dans la requête
        $donnees = json_decode($request->getContent(), true);

        // Vérifier si les données sont valides
        if (!isset($donnees['choix']) || !isset($donnees['key'])) {
            return new JsonResponse(['error' => 'Données invalides : choix et clé requis'], 400);
        }

        // Extraire le choix de l'utilisateur et la clé
        $choixUtilisateur = $donnees['choix'];
        $key = $donnees['key'];

        // Valider les données (exemple : vérification de la validité du choix)
        if (!in_array($key, ['logement_type', 'logement_construction', 'logement_energie'])) {
            return new JsonResponse(['error' => 'Choix invalide'], 400);
        }

        // Enregistrer le choix de l'utilisateur dans la session Symfony
        $request->getSession()->set($key, $choixUtilisateur);

        // Répondre avec une réponse JSON pour indiquer le succès
        return new JsonResponse(['success' => true]);
    }

    #[Route('/logement-type', name: 'logement_type')]
    public function logement_type(Request $request): Response
    {
        // Retourne la vue correspondante
        return $this->render('simulateur/logement_type.html.twig');
    }

    #[Route('/logement-construction', name: 'logement_construction')]
    public function logement_construction(Request $request): Response
    {
        // Enregistrer le choix de l'utilisateur dans la session Symfony
        $request->getSession()->set('logement_construction', 'maison_pas_vielle');

        // Retourne la vue correspondante
        return $this->render('simulateur/logement_construction.html.twig');
    }

    #[Route('/logement-surface', name: 'logement_surface')]
    public function logement_surface(Request $request): Response
    {
        // Enregistrer le choix de l'utilisateur dans la session Symfony
        $request->getSession()->set('logement_surface', 'maison_petite');

        // Retourne la vue correspondante
        return $this->render('simulateur/logement_surface.html.twig');
    }
}
